"""This is my solution to day 1 part 1, which two numbers in this list sum to 2020"""

import math

# Read the files in as ints
with open("exp_report.csv") as f:
    exp = set(int(num) for num in f.readlines())
# Get a set of the differences between 2020 and all numbers in the set
remainders = set(2020 - i for i in exp)

# Get the values in both sets
candidates = exp & remainders

# Print the product of all the candidates, aka the answer
print(math.prod(candidates))
