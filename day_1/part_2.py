"""This is my solution to day 1 part 2, which three numbers in this list sum to 2020"""


import math

# Read the files in as ints
with open("exp_report.csv") as f:
    exp = set(int(num) for num in f.readlines())

# Get a set of the differences between 2020 and all numbers in the set
remainders_1 = set(2020 - i for i in exp)


# Make an empty set
remainders_2 = set()

# Get all the differences between all initiall diffs and the original set, in all permutations
for x in remainders_1:
    for y in exp:
        remainders_2.update(x - y)

# Get the values in both sets
candidates = exp & remainders_2

# Print the product of all the candidates, aka the answer
print(math.prod(candidates))
